//
//  CustomButton.swift
//  custom touch area test
//
//  Created by Mark Senich on 4/15/19.
//  Copyright © 2019 Austin senich. All rights reserved.
//

import UIKit

extension UIView {
    func alphaFromPoint(point: CGPoint) -> CGFloat {
        var pixel: [UInt8] = [0,0,0,0]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let alphaInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: &pixel,width: 1,height: 1,bitsPerComponent: 8,bytesPerRow: 4, space: colorSpace, bitmapInfo: alphaInfo.rawValue)
        
        context!.translateBy(x:-point.x, y: -point.y)
        
        self.layer.render(in: context!)
        
        let floatAlpha = CGFloat(pixel[3])
        return floatAlpha
    }
}


 @IBDesignable class customButton: UIButton {
    @IBInspectable var threshold: CGFloat = 1.0
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return self.alphaFromPoint(point: point) > threshold
    }
    
    
    @IBInspectable var customRotation: CGFloat = 0 {

        didSet {
            self.transform = CGAffineTransform(rotationAngle: customRotation)
        }
    }
    
}
